######################
# Main Control Clock #
######################
create_clock -name clk_fpga_0 -period 10.000 \
  [get_pins -hier * -filter {NAME =~ */ps/U0/PS8_i/PLCLK[0]}]
set_property DONT_TOUCH true \
  [get_cells -hier * -filter {NAME =~ */ps/U0/PS8_i}]

##################
# SFP+ - Bank 49 #
##################
set_property PACKAGE_PIN A12      [get_ports SFP0_TX_DISABLE]
set_property IOSTANDARD  LVCMOS33 [get_ports SFP0_TX_DISABLE]
set_property SLEW        SLOW     [get_ports SFP0_TX_DISABLE]
set_property DRIVE       8        [get_ports SFP0_TX_DISABLE]

###################
# SFP+ - Bank 230 #
###################
set_property PACKAGE_PIN D1 [get_ports SFP_RX0_N]
set_property PACKAGE_PIN D2 [get_ports SFP_RX0_P]
set_property PACKAGE_PIN E3 [get_ports SFP_TX0_N]
set_property PACKAGE_PIN E4 [get_ports SFP_TX0_P]

set_property PACKAGE_PIN C7 [get_ports SFP_REFCLK_N]
set_property PACKAGE_PIN C8 [get_ports SFP_REFCLK_P]
create_clock -period 6.400 -name sfp_refclk_p [get_ports SFP_REFCLK_P]

