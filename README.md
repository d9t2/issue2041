# OpenCPI Issue 2041

This repository demonstrates Issue [#2041](https://gitlab.com/opencpi/opencpi/-/issues/2041) in OpenCPI.

To use this repository, simply:
```bash
  ./build.sh
```
This script will register the project, build the platform, and build the assembly core.
It will then terminate `ocpidev build` sometime after the config core is generated.

Then it looks for strings in the config core `-assy.vhd` file that should be there, but they aren't.
Then it looks for strings in the config core `-assy.vhd` file that shouldn't be there, but they are.
